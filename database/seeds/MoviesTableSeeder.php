<?php

use Illuminate\Database\Seeder;
use App\Movies;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Movies::create(['name'=>'Avengers']);
        Movies::create(['name'=>'Venom']);
        Movies::create(['name'=>'Justice League']);
        Movies::create(['name'=>'Aquaman']);
    }
}
