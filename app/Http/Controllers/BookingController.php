<?php

namespace App\Http\Controllers;
use App\Booking;
use App\Movies;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    //

    public function getMovies(){
        $movies=Movies::get();
        return response()->json(['success'=>true,'movies'=>$movies],200);

    }
    public function booknow(Request $request){
        $user_id=Auth::user()->id;
        $booking=new Booking();
        $booking->user_id=$user_id;
        $booking->movie_id=$request->movie_id;
        $booking->save();
        return response()->json(['success'=>true,'message'=>'Booking Placed'],200);
    }
    public function getBookings(){
        $user_id=Auth::user()->id;
        $bookings=Booking::where('user_id',$user_id)->get();
        return response()->json(['success'=>true,'bookings'=>$bookings],200);
    }
}
